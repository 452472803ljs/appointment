import { useEffect, useState } from "react";
import "./App.css";
import CardsContainer from "./components/cardContainer/CardsContainer";
import PopupModal from "./components/popUpModal/PopupModal";
function App() {
  const [addAppointment, setAddAppointment] = useState(false);
  const [addGuests, setGuests] = useState(false);
  const [addStaffs, setStaffs] = useState(false);
  const [addServices, setServices] = useState(false);

  const openModal = (title) => {
    title === "Appointments" && setAddAppointment(!addAppointment);
    title === "Guests" && setGuests(!addGuests);
    title === "Staffs" && setStaffs(!addStaffs);
    title === "Services" && setServices(!addServices);
  };

  const turnOffModal = () => {
    setAddAppointment(false);
    setGuests(false);
    setStaffs(false);
    setServices(false);
  };
  const dataSet = {
    Guests: [
      {
        name: "Paula Thompson",
        phone: "111-111-1111",
        email: "paula@test.com",
      },
      {
        name: "Tim Lightner",
        phone: "111-111-2222",
        email: "paula@test.com",
      },
      {
        name: "Michael Doherty",
        phone: "111-111-3333",
        email: "paula@test.com",
      },
      {
        name: "Susan Carlson",
        phone: "111-111-4444",
        email: "paula@test.com",
      },
    ],
    Staffs: [
      { name: "Alicia Pierce", position: "Manager" },
      { name: "Ronald Klein", position: "Hair Stylist" },
    ],
    Services: [
      { name: "Mens Haircut", type: "Haircut", price: 20 },
      { name: "Womens Haircut", type: "Haircut", price: 20 },
      { name: "Color", type: "Color", price: 40 },
    ],
    Appointments: [
      {
        name: "Paula Thompson",
        timeSlot: "4:00pm - 4:30pm",
        customer: "Ronald Klein",
        service: "Mens HairCut",
      },
      {
        name: "Susan Carlson",
        timeSlot: "6:00pm - 7:30pm",
        customer: "Ronald Klein",
        service: "Color",
      },
      {
        name: "Paula Thompson",
        timeSlot: "4:00pm - 4:30pm",
        customer: "Ronald Klein",
        service: "Mens HairCut",
      },
      {
        name: "Susan Carlson",
        timeSlot: "6:00pm - 7:30pm",
        customer: "Ronald Klein",
        service: "Color",
      },
      {
        name: "Paula Thompson",
        timeSlot: "4:00pm - 4:30pm",
        customer: "Ronald Klein",
        service: "Mens HairCut",
      },
      {
        name: "Susan Carlson",
        timeSlot: "6:00pm - 7:30pm",
        customer: "Ronald Klein",
        service: "Color",
      },
    ],
  };

  return (
    <div className="App">
      <CardsContainer
        title="Guests"
        data={dataSet.Guests}
        openModal={openModal}
      />
      <CardsContainer
        title="Staffs"
        data={dataSet.Staffs}
        openModal={openModal}
      />
      <CardsContainer
        title="Services"
        data={dataSet.Services}
        openModal={openModal}
      />
      <CardsContainer
        title="Appointments"
        flexGrow="1"
        data={dataSet.Appointments}
        openModal={openModal}
      />
      {addAppointment && dataSet && (
        <PopupModal
          turnOffModal={turnOffModal}
          title="Add Appointments"
          dataSet={dataSet}
        />
      )}
      {addGuests && (
        <PopupModal turnOffModal={turnOffModal} title="Add Guests" />
      )}
      {addStaffs && (
        <PopupModal turnOffModal={turnOffModal} title="Add Staffs" />
      )}
      {addServices && (
        <PopupModal turnOffModal={turnOffModal} title="Add Services" />
      )}
    </div>
  );
}

export default App;
