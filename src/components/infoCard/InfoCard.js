import styles from "./InfoCard.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPhone,
  faEnvelope,
  faClock,
  faUser,
  faLeaf,
} from "@fortawesome/free-solid-svg-icons";

const InfoCard = (props) => {
  const {
    name,
    phone,
    email,
    position,
    type,
    price,
    timeSlot,
    customer,
    service,
  } = props.data;

  return (
    <div className={styles.container}>
      <div className={styles.title}>{name}</div>
      <div className={styles.iconAndInfos}>
        {timeSlot && (
          <>
            <div className={styles.iconInfoContainer}>
              <span className={styles.icon}>
                <FontAwesomeIcon icon={faClock}>/</FontAwesomeIcon>
              </span>
              <span>{timeSlot}</span>
            </div>
            <div className={styles.iconInfoContainer}>
              <span className={styles.icon}>
                <FontAwesomeIcon icon={faLeaf}>/</FontAwesomeIcon>
              </span>
              <span>{service}</span>
            </div>
            <div className={styles.iconInfoContainer}>
              <span className={styles.icon}>
                <FontAwesomeIcon icon={faUser}>/</FontAwesomeIcon>
              </span>
              <span>{customer}</span>
            </div>
          </>
        )}
        {phone && (
          <div className={styles.iconInfoContainer}>
            <span className={styles.icon}>
              <FontAwesomeIcon icon={faPhone}>/</FontAwesomeIcon>
            </span>
            <span>111-111-1111</span>
          </div>
        )}

        {email && (
          <div className={styles.iconInfoContainer}>
            <span className={styles.icon}>
              <FontAwesomeIcon icon={faEnvelope}>/</FontAwesomeIcon>
            </span>
            <span>452472803ljs@gmail.com</span>
          </div>
        )}
      </div>
      {position && (
        <div className={styles.otherInfo}>
          <ul>
            <li>Manager</li>
          </ul>
        </div>
      )}

      {type && (
        <div className={styles.otherInfo}>
          <ul>
            <li>{type}</li>
            <li>{price}</li>
          </ul>
        </div>
      )}
    </div>
  );
};

export default InfoCard;
