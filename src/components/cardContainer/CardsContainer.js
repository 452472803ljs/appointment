import styles from "./CardsContainer.module.scss";
import InfoCard from "../infoCard/InfoCard";

const CardsContainers = (props) => {
  const { title, flexGrow, data, openModal } = props;
  const changeModal = () => {
    console.log(title);
    title === "Appointments" && openModal(title);
    title === "Guests" && openModal(title);
    title === "Staffs" && openModal(title);
    title === "Services" && openModal(title);
  };
  return (
    <div className={styles.cardsContainer} style={{ flexGrow }}>
      <div className={styles.title}>{title}</div>

      <div className={styles.content}>
        {data &&
          data?.map((eachObj) => {
            return <InfoCard data={eachObj} />;
          })}
      </div>
      <div className={styles.addNew} onClick={changeModal}>
        +
      </div>
    </div>
  );
};

export default CardsContainers;
