import styles from "./PopupModal.module.scss";
import SelectBar from "../selectBar/SelectBar";

const PopupModel = ({ turnOffModal, title, dataSet }) => {
  console.log(dataSet);
  return (
    <div className={styles.container}>
      <div className={styles.content}>
        <div className={styles.title}>{title}</div>
        {title === "Add Appointments" && (
          <div className={styles.innerContainer}>
            <div className={styles.selections}>
              <SelectBar placehold="Guests" dataSet={dataSet.Guests} />
              <SelectBar placehold="Staffs" dataSet={dataSet.Staffs} />
              <SelectBar placehold="Services" dataSet={dataSet.Services} />
            </div>
          </div>
        )}
        {title === "Add Guests" && (
          <div className={styles.innerContainer}>
            <div className={styles.input}>
              <label for="firstName">First Name:</label>
              <input type="text" id="firstName" name="firstName" />
            </div>
            <div className={styles.input}>
              <label for="lastName">Last Name:</label>
              <input type="text" id="lastName" name="lastName" />
            </div>

            <div className={styles.input}>
              <label for="email">Email:</label>
              <input type="text" id="email" name="email" />
            </div>

            <div className={styles.input}>
              <label for="phone">Phone:</label>
              <input type="phone" id="phone" name="phone" />
            </div>
          </div>
        )}

        {title === "Add Staffs" && (
          <div className={styles.innerContainer}>
            <div className={styles.input}>
              <label for="firstName">First Name:</label>
              <input type="text" id="firstName" name="firstName" />
            </div>
            <div className={styles.input}>
              <label for="lastName">Last Name:</label>
              <input type="text" id="lastName" name="lastName" />
            </div>

            <div className={styles.input}>
              <label for="position">Position:</label>
              <input type="text" id="position" name="position" />
            </div>
          </div>
        )}

        {title === "Add Services" && (
          <div className={styles.innerContainer}>
            <div className={styles.input}>
              <label for="service">Service:</label>
              <input type="text" id="service" name="service" />
            </div>
            <div className={styles.input}>
              <label for="type">Type:</label>
              <input type="text" id="type" name="type" />
            </div>

            <div className={styles.input}>
              <label for="price">Price:</label>
              <input type="text" id="price" name="price" />
            </div>
          </div>
        )}

        <div className={styles.buttons}>
          <button>CONFIRM</button>
          <button onClick={turnOffModal}>Exit</button>
        </div>
      </div>
    </div>
  );
};

export default PopupModel;
