import { useState } from "react";
import styles from "./SelectBar.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
const SelectBar = ({ placehold, dataSet }) => {
  const [selected, setSelected] = useState();
  const handleChange = (event) => {
    setSelected(event.target.value);
  };
  return (
    <div className={styles.dropdown}>
      <div className={styles.dropdownSelect}>
        <span className={styles.select}>{placehold}</span>
        <span className={styles.icon}>
          <FontAwesomeIcon icon={faArrowDown}>/</FontAwesomeIcon>
        </span>
      </div>
      <div className={styles.dropdownList}>
        {dataSet &&
          dataSet.map((data) => (
            <div className={styles.dropdownListItem}>{data.name}</div>
          ))}
      </div>
    </div>
  );
};
export default SelectBar;
